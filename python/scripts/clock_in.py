'''
Created on 22 Aug 2019

@author: hemanthv
'''
'''
Created on 16 Aug 2019

@author: hemanthv
@version: 0.0.1
'''

from selenium import webdriver
import getpass
import time
import platform
import sys
import os
from pykeepass import PyKeePass
from selenium.webdriver.common.action_chains import ActionChains
import random

import startup



'''
This is to clock in

@author: hemanthv
'''
def try_clocking_in(location):
    try:
        driver = webdriver.Chrome(location)
        driver.get('https://onecampus.vt.edu/launch-task/all/timeclock-042113?roles=')
    except:
        print("Driver path error occured." + location + " does not exist")
        exit()
     
      
    
    
    
    try: 
        kp = PyKeePass('../../authorization.kdbx', password='password')
        entry = kp.find_entries(title='generic_mail', first=True)
        
        
        username_area = driver.find_element_by_id("username")
        username_area.send_keys(entry.username)
        
        password_area = driver.find_element_by_id("password")
        password_area.send_keys(entry.password)
    
        submit_button = driver.find_element_by_name("_eventId_proceed")
        submit_button.click()
    
        timeout = 60
        i = 0
        while i < timeout:
            if "Login with 2-Factor" in driver.page_source:
                break
            time.sleep(0.5)
            i += 1
        
        
        try:
            duo_push_button = driver.find_elements_by_css_selector("/#auth_methods > fieldset:nth-child(1) > div.row-label.push-label > button")  
            duo_push_button = driver.find_element_by_xpath("//*[@id=\"auth_methods\"]/fieldset[1]/div[1]/button")
            duo_push_button.click()
        except:
            print("Duo push is probably set up as a default method of authentication")
    
        duo_timeout = 30    # Duo timeout (in seconds)
        i = 0
        while i < duo_timeout:
            if "Clocked" in driver.page_source:
                print("I see the dashboard")
                break
            time.sleep(1)
            i += 1
            
        
        
        try:
            clock_in_button = driver.find_element_by_id("ClockIn")
            clock_in_button.click()
        
            
            i = 0
            while i < timeout:
                if "Confirmation (Clock In)" in driver.page_source:
                    break
                time.sleep(0.5)
                i += 1
            
            
            continue_button = driver.find_element_by_xpath("/html[@class='ng-scope']/body[@class='CssLoaded ng-scope HideVirtualKeyboard']/div[@class='LayoutWrapper']/main[@class='LayoutBodyContainer ng-scope']/div[@class='ClockOperation LayoutBody']/div[@class='LayoutContentContainer']/div[@class='FeatureContentContainer']/form[@id='featureForm']/div[@class='ng-scope']/div[@class='ng-scope']/div[@class='ng-scope']/div[@class='ClockOperationContentContainer']/div[@class='ClockOperationButtonContainer']/input[@class='tcp-btn BtnAction DefaultSubmitBehavior']")
            continue_button.click()
            
            
            
            try:
                i = 0
                while i < timeout:
                    if "Select Job Code (Clock In)" in driver.page_source:
                        break
                    time.sleep(0.5)
                    i += 1
                
                driver.find_element_by_xpath("/html[@class='ng-scope']/body[@class='CssLoaded ng-scope HideVirtualKeyboard']/div[@class='LayoutWrapper']/main[@class='LayoutBodyContainer ng-scope']/div[@class='ClockOperation LayoutBody']/div[@class='LayoutContentContainer']/div[@class='FeatureContentContainer']/form[@id='featureForm']/div[@class='ng-scope']/div[@class='ng-scope']/div[@class='ng-scope']/div[@class='ClockOperationContentContainer']/div[@class='ClockOperationButtonContainer']/input[@class='tcp-btn BtnAction DefaultSubmitBehavior']").click()
            
            except:
                print("You probably have just one job code")
            
            print("[SUCCESS]    " + random.choice(startup.clockinphrases))
            
        except:
            print("Are you already clocked in?") 
            driver.close()
            driver.quit()           
    
    except:
        print("Login authentication failed")
     
    
    driver.close()
    driver.quit()

def main():
    startup.main()
    location = startup.location
    try_clocking_in(location)
    


if __name__ == "__main__":
    main()
    