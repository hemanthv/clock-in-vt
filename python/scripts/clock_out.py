'''
Created on 16 Aug 2019

@author: hemanthv
'''
from selenium import webdriver
import getpass
import time
import platform
import sys
import os
from selenium.webdriver.common.action_chains import ActionChains
import random
from pykeepass import PyKeePass

import startup

def try_clocking_out(location):
    try:
        driver = webdriver.Chrome(location)
        driver.get('https://onecampus.vt.edu/launch-task/all/timeclock-042113?roles=')
    except:
        print("Driver path error occured.\n" + location + " does not exist")
        exit()
    
    try: 
        kp = PyKeePass('../../authorization.kdbx', password='password')
        entry = kp.find_entries(title='generic_mail', first=True)
        
        username_area = driver.find_element_by_id("username")
        username_area.send_keys(entry.username)
            
        password_area = driver.find_element_by_id("password")
        password_area.send_keys(entry.password)
        
        submit_button = driver.find_element_by_name("_eventId_proceed")
        submit_button.click()
        
        timeout = 60
        i = 0
        while i < timeout:
            if "Login with 2-Factor" in driver.page_source:
                break
            time.sleep(0.5)
            i += 1
        
         
        try: 
            duo_push_button = driver.find_elements_by_css_selector("/#auth_methods > fieldset:nth-child(1) > div.row-label.push-label > button")  
            duo_push_button = driver.find_element_by_xpath("//*[@id=\"auth_methods\"]/fieldset[1]/div[1]/button")
            duo_push_button.click()
        except:
            print("Duo push is probably set up as a default method of authentication")
        
        
        duo_timeout = 30
        i = 0
        while i < duo_timeout:
            if "Clocked" in driver.page_source:
                print("I see the dashboard")
                break
            time.sleep(1)
            i += 1
        
        
        try:
            clock_in_button = driver.find_element_by_xpath("/html[@class='ng-scope']/body[@class='CssLoaded ng-scope HideVirtualKeyboard']/div[@class='LayoutWrapper']/div[@class='LayoutHeader ng-scope']/div[@class='PageHeaderContainer ng-scope']/header[@class='ng-scope']/div[@class='ng-scope']/div[@class='HeaderMenuContainer ng-scope']/nav[@class='RootMenuContainer']/ul[@id='appmenu']/li[@id='ClockOut']/div[@class='RootMenuItem ng-binding ng-scope']")
            clock_in_button.click()
        
            time.sleep(3) 
            
            continue_button = driver.find_element_by_xpath("/html[@class='ng-scope']/body[@class='CssLoaded ng-scope HideVirtualKeyboard']/div[@class='LayoutWrapper']/main[@class='LayoutBodyContainer ng-scope']/div[@class='ClockOperation LayoutBody']/div[@class='LayoutContentContainer']/div[@class='FeatureContentContainer']/form[@id='featureForm']/div[@class='ng-scope']/div[@class='ng-scope']/div[@class='ng-scope']/div[@class='ClockOperationContentContainer']/div[@class='ClockOperationButtonContainer']/input[@class='tcp-btn BtnAction DefaultSubmitBehavior']")
            continue_button.click()
            #time.sleep(40)
        
            print("[SUCCESS]    " + random.choice(startup.clockout_phrases))  
        except:
            print("You probably weren\'t clocked in")
    except:
        print("The login authentication had a problem")  
        
    
    driver.close()
    driver.quit()
    #"html[@class='ng-scope']/body[@class='CssLoaded ng-sc ope HideVirtualKeyboard']/div[@class='LayoutWrapper']/main[@class='LayoutBodyContainer ng-scope']/div[@class='ClockOperation LayoutBody']/div[@class='LayoutContentContainer']/div[@class='FeatureContentContainer']/form[@id='featureForm']/div[@class='ng-scope']/div[@class='ng-scope']/div[@class='ng-scope']/div[@class='ClockOperationContentContainer Highlight']/div[@class='ClockOperationButtonContainer']/input[@class='tcp-btn BtnAction DefaultSubmitBehavior']")


def main():
    startup.main()                  # This is to setup the driver location
    location = startup.location     # Getting the set location from startup.py
    try_clocking_out(location)      # The main method
    


if __name__ == "__main__":
    main()