'''
Created on 22 Aug 2019

@author: hemanthv
'''

from selenium import webdriver
import getpass
import time
import platform
from pykeepass import PyKeePass
#from __builtin__ import False

location = ""


clockinphrases = ['We clocked in fam', 
                  'We in buddy', 
                  'We in', 
                  'The eagle has landed? I don\'t know. You are clocked in', 
                  'The awesome script that I made clocked you in. I\'m the best',
                  'Do your job properly. You are getting paid now.',
                  'Cha-Ching! You are clocked in and getting paid',
                  'We are in',
                  'Jarvis clocked you in']


clockout_phrases = ['Obama out', 
                    'Work must have been stressful. Have a drink. You\'re clocked out', 
                    'Clocked out',
                    'Jarvis clocked you out',
                    'CLocking you out']

LoginTime=0



'''
This is to test the driver

@author: hemanthv
'''
def testDriver():
    global location
    try:
        driver = webdriver.Chrome(location)
        driver.get('https://www.google.com/search?client=opera&q=good+job+fam&sourceid=opera&ie=UTF-8&oe=UTF-8')#('https://onecampus.vt.edu/task/all/timeclock-042113')
        time.sleep(3)
        driver.close()
        driver.quit()
        time.sleep(1)
        print("So the driver seems to work")
    except:
        print("Driver path error occured.\n", location, " does not exist")
        

def fillCredentials():
    kp = PyKeePass('../../authorization.kdbx', password='password')
    
    group = kp.add_group(kp.root_group, 'email')
    
    username = input('Enter username: ')
    password = getpass.getpass(prompt='Enter password: ')
    

    if login_attempt(username, password):
        kp.add_entry(group, 'generic_mail', 
                        username,
                        password)
        print("Login credentials added.")
        print("Ignore the Duo Push.")
        kp.save()
    else:
        print("Try running the script again.")


def login_attempt(username, password):
    global location
    try:
        driver = webdriver.Chrome(location)
        driver.get('https://onecampus.vt.edu/launch-task/all/timeclock-042113?roles=')
    except:
        print("Driver path error occured." + location + " does not exist")
        exit()
    
    
    try:
        username_area = driver.find_element_by_id("username")
        username_area.send_keys(username)
        
        password_area = driver.find_element_by_id("password")
        password_area.send_keys(password)   #Wrong password
    
        submit_button = driver.find_element_by_name("_eventId_proceed")
        submit_button.click()
    except:
        print("Logging in had some issues")
    
    
    if ("Invalid username or password." in  driver.page_source):
        print("Username or password entered was wrong")
        return False
    return True

def main():
    
    global location
    
    if platform.system().lower() == "windows":
        location = "C:\\Users\\" + getpass.getuser() + "\\Downloads\\chromedriver.exe"   
    elif platform.system().lower() == "darwin":
        location = "/Users/" + getpass.getuser() + "/Downloads/chromedriver"
    elif platform.system().lower() == "linux":
        location = "/Users/" + getpass.getuser() + "/Downloads/chromedriver"
    else:
        print("Either we can\'t find os information or the driver isn\'t in the Downloads folder")
        location = input("Enter driver location here: ")
    
    print("Detected kernel: " + platform.system())
    
    kp = PyKeePass('../../authorization.kdbx', password='password')
    
    if len(kp.find_entries(title='generic_mail')) == 0:
        fillCredentials()
    
  
if __name__ == "__main__":
    main()