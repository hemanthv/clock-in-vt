# Setting up the project

1. Download the [chrome webdriver](https://chromedriver.chromium.org/downloads) to facilitate automation in your laptop and leave it in your downloads folder.
2. Pull project and run needed script (clock_in.py or clock_out.py)
3. Install the packages in the requirements.txt file
4. For the first time, you will need to enter your credentials

Mac/Linux users:
```
git clone code.vt.edu/hemanthv/clock-in-vt
pip install --requirement requirements.txt
cd python
cd scripts
python clock_in.py
(or) 
python clock_out.py
```

# Project Restrictions

1. "Duo push" needs to be the default method of 2-factor authentication
2. Clocking in with multiple job codes is still under testing